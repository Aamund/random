let dreams = [];
let stars = [];
let dusts = [];
let r, g, b;
let executeUniverse = false;
let isOverButton = false
let theButtonHasBeenPressed = false;
let buttonSizeX = 200
let buttonSizeY = 50
let percent = 0
let percentCol = 0
let buttonScale = 1
let t;
let sleepNow = false
let col;
let img

function preload() {

myFont = loadFont('Michroma.ttf');
img = loadImage('DreamViewLogo.png');
}

function setup() {
  createCanvas(1440, 900);
  // dreams = new Dream();

  col = color(231,130,0);


  r = random(255);
  g = random(255);
  b = random(255);

  textFont(myFont);

  for (let i = 0; i < 10; i++) {
    let x = random(width);
    let y = random(250, 500);
    let r = random(20, 60);
    let d = new Dream(x, y, r);
    dreams.push(d);
  }
  }


function draw() {
  background(0);
  noFill();
  stroke(255);




  // if (mouseX > windowWidth/2-100 && mouseX < windowWidth/2+100 && mouseY < windowHeight-100 && mouseY > windowHeight-50)


  // mouseX >= x && mouseX <= x+width && mouseY >= y && mouseY <= y+height
    if (mouseX >= 620 && mouseX <= 620+200 && mouseY >= 785 && mouseY <= 785+50)
{
  isOverButton = true
  // background(255)
} else {
  isOverButton = false
  // background(0)
}




  for (let i = 0; i < dreams.length; i++) {
    dreams[i].move();
    dreams[i].show();

  }

  beginShape();
  for (var x = 0; x < width; x++) {
	var nx = map(x, 0, width, 0, 10);
	var y = height * noise(nx);
	vertex(x, y);
  }
  endShape();

  strokeWeight(1);
  stroke(231,130,0);
    line(0,450,1440,450)
    line(720, 400, 720, 500)
    line(540, 400, 540, 500)
    line(360, 400, 360, 500)
    line(180, 400, 180, 500)
    line(900, 400, 900, 500)
    line(1080, 400, 1080, 500)
    line(1260, 400, 1260, 500)

    ellipse(720,450,50,50);
    ellipse(540,450,50,50);
    ellipse(360,450,50,50);
    ellipse(180,450,50,50);
    ellipse(900,450,50,50);
    ellipse(1080,450,50,50);
    ellipse(1260,450,50,50);

    fill(255);
    textSize(20);
    text("05", 700, 457);
    text("04", 520, 457);
    text("03", 340, 457);
    text("02", 160, 457);
    text("06", 880, 457);
    text("07", 1060, 457);
    text("08", 1240, 457);

fill(29, 42, 56)
noStroke();
rect(250, 725, 400, 200)
stroke(150);
strokeWeight(1);
line(50, 775, 450, 775);
line(50, 725, 450, 725);
line(50, 675, 450, 675);
fill(200);
textSize(16);
text("Sleep time:                                       01:23 - 8:30", 55, 670);
text("Sleep quality:                                67 percent", 55, 720);
text("Time in bed:                                    7:07", 55, 770);
text("Sleep keyword:                          Unstable", 55, 820);









fill(col);
noStroke();
  rectMode(CENTER);
  rect(720, 800, 200, 50);


push();
noStroke();
fill(255);
textSize(20);
text("Load Dream", 640, 805)
fill(231,130,0);
textSize(50);
text("Choose your dream", 420, 100)
textSize(30);
text("01 - 02.12.19", 420, 150)
pop();


if (theButtonHasBeenPressed == true) {
  console.log(theButtonHasBeenPressed);
col = color(255);
  for (var u = 0; u < 1; u++) {
  buttonSizeX = buttonSizeX + 20
  buttonSizeY = buttonSizeY +20

  if (buttonSizeX > 2000 && buttonSizeY > 2000) {
    buttonSizeX = 2000
    buttonSizeY = 2000
    console.log("button")


  }

}


executeUniverse = true;

if (percent > 100) {
  sleepNow = true
percentCol = 255
executeUniverse = false
console.log("FALSE");


}
}

if (sleepNow == true) {

window.open("https://cdn.statically.io/gl/Aamund/random/master/SF_v2/index.html");
sleepNow = false

}


// if (sleepNow == true) {
//    background(0, 10);
//
//
//    for (var x = 0; x < width; x+=100) {
//     for (var y = 0; y < height; y+=100) {
//
//       rect(x, y, 100, 100);
//     }
//     }
//
//
//    var x1 = 600 * noise(t + 15);
//    var x2 = 600 * noise(t + 25);
//    var x3 = 600 * noise(t + 35);
//    var x4 = 600 * noise(t + 45);
//    var y1 = 600 * noise(t + 55);
//    var y2 = 600 * noise(t + 65);
//    var y3 = 600 * noise(t + 75);
//    var y4 = 600 * noise(t + 85);
//
//  fill(200)
//
//    bezier(x1, y1, x2, y2, x3, y3, x4, y4);
//
//
//    t += 0.005;
//
//    fill(255,0,0,0)
//    rect(0,0,100,100);
//
//    // fill(130,29,45)
//    // rect(0, 0,4000,4000)
//
//    console.log("sleeping");
//
// }





  if (executeUniverse == true) {

    rect(720, 800, buttonSizeX, buttonSizeY);


    for (var i = 0; i < 5; i++) {
     dusts.push(new Dust());
    }

    //pushes new stars into the array
    for (var i = 0; i < 1; i++) {
     stars.push(new Star());
    }

    //draws the dust
    for (var i = 0; i < dusts.length; i++) {
     dusts[i].draw();
    }

    //draws the stars
    for (var i = 0; i < stars.length; i++) {
     stars[i].draw();
    }

    percent = percent + 0.2
    fill(percentCol)
    text(percent, 720, 800);




  }

  image(img, 50, -20, 200, 200)


}


  function mousePressed() {
    for (let i = 0; i < dreams.length; i++) {
      dreams [i].clicked(mouseX, mouseY);
    }
    if (isOverButton == true){
      //pushes new dust into the array
theButtonHasBeenPressed = true;

    }
    console.log(mouseX, mouseY)


  }


class Dream {
  constructor(x, y, r) {
    this.x = x;
    this.y = y;
    this.r = r;
    this.brightness = 0;
  }

  clicked(px, py) {
    let d = dist(px, py, this.x, this.y);
    if (d < this.r) {
      this.brightness = 255;
    }
  }

  move() {
    this.x = this.x + random(-2, 2);
    this.y = this.y + random(-2, 2);
  }

  show() {
    stroke(255);
    strokeWeight(4);
    fill(this.brightness, 125);
    ellipse(this.x, this.y, this.r * 2);
  }
}

function Universe() {


}

//the stars and their properties
function Star() {
  this.w = 5;
  this.h = 5;
  this.x = random(windowWidth);
  this.y = random(windowHeight);
  this.f = (r,g,b);



}

//the dust and their properties
function Dust() {
  this.w = 1;
  this.h = 1;
  this.x = random(windowWidth);
  this.y = random(windowHeight);


}

function Sleep() {

 //  background(255, 10);
 //
 //   for (var x = 0; x < width; x+=100) {
 // 		for (var y = 0; y < height; y+=100) {
 //
 // 			rect(x, y, 100, 100);
 // 		}
 //   	}
 //
 //
 //   var x1 = 100 * noise(t + 15);
 //   var x2 = 100 * noise(t + 25);
 //   var x3 = 100 * noise(t + 35);
 //   var x4 = 100 * noise(t + 45);
 //   var y1 = 100 * noise(t + 55);
 //   var y2 = 100 * noise(t + 65);
 //   var y3 = 100 * noise(t + 75);
 //   var y4 = 100 * noise(t + 85);
 //
 // fill(200)
 //
 //   bezier(x1, y1, x2, y2, x3, y3, x4, y4);
 //
 //
 //   t += 0.005;
 //
 //   fill(255,0,0,0)
 //   rect(0,0,100,100);

   // clear the background every 500 frames using mod (%) operator
   // if (frameCount % 500 == 0) {
 	// background(255);
   // }
 }





//what the stars are doing
Star.prototype.draw = function() {
  noStroke();
  //fill(this.f);
  fill(random(238),random(232),random(170));
  ellipse(this.x, this.y, this.w, this.h);
  this.x += (random(2) - 1);
  this.y += (random(2) - 1);

  if (this.w == 5) {
    this.w = 5;
    this.h = 5;
  } else {
    this.w = 3;
    this.h = 3;
  }

}

//what the dust is doing
Dust.prototype.draw = function() {
  noStroke();
  fill(0);
  ellipse(this.x, this.y, this.w, this.h);
  this.x += (random(2) - 1);
  this.y += (random(2) - 1);

  if (this.w == 1) {
    this.w = 2;
    this.h = 2;
  } else {
    this.w = 1;
    this.h = 1;
  }

}
