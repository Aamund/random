let stars = [];
let dusts = [];
let r, g, b;


function setup() {
  createCanvas(windowWidth, windowHeight);


  r = random(255);
  g = random(255);
  b = random(255);

frameRate(10);


}

function draw() {
  background(0);

  //pushes new dust into the array
   for (var i = 0; i < 5; i++) {
    dusts.push(new Dust());
  }

  //pushes new stars into the array
  for (var i = 0; i < 1; i++) {
    stars.push(new Star());
  }

  //draws the dust
  for (var i = 0; i < dusts.length; i++) {
    dusts[i].draw();
  }

  //draws the stars
  for (var i = 0; i < stars.length; i++) {
    stars[i].draw();
  }
}

//the stars and their properties
function Star() {
  this.w = 5;
  this.h = 5;
  this.x = random(windowWidth);
  this.y = random(windowHeight);
  this.f = (r,g,b);



}

//the dust and their properties
function Dust() {
  this.w = 1;
  this.h = 1;
  this.x = random(windowWidth);
  this.y = random(windowHeight);


}

//what the stars are doing
Star.prototype.draw = function() {
  noStroke();
  //fill(this.f);
  fill(random(238),random(232),random(170));
  ellipse(this.x, this.y, this.w, this.h);
  this.x += (random(2) - 1);
  this.y += (random(2) - 1);

  if (this.w == 5) {
    this.w = 5;
    this.h = 5;
  } else {
    this.w = 3;
    this.h = 3;
  }

}

//what the dust is doing
Dust.prototype.draw = function() {
  noStroke();
  fill(255);
  ellipse(this.x, this.y, this.w, this.h);
  this.x += (random(2) - 1);
  this.y += (random(2) - 1);

  if (this.w == 1) {
    this.w = 2;
    this.h = 2;
  } else {
    this.w = 1;
    this.h = 1;
  }

}
