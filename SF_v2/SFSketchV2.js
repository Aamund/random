var t;
var img;
var myFont;
var mig;

function preload() {
  mig = loadImage('DreamViewLogo.png');
   img = loadImage('shareicon.png');
   myFont = loadFont('Michroma.ttf');
}

function setup() {


  let cnv = createCanvas(windowWidth, windowHeight);
  var x = (windowWidth - width) / 2;
  var y = (windowHeight - height) / 2;
  cnv.position(x, y);
  stroke(0, 18);
  noFill();
  t = 0;





}

function draw() {





  var x1 = width * noise(t + 15);
  var x2 = width * noise(t + 25);
  var x3 = width * noise(t + 35);
  var x4 = width * noise(t + 45);
  var y1 = height * noise(t + 55);
  var y2 = height * noise(t + 65);
  var y3 = height * noise(t + 75);
  var y4 = height * noise(t + 85);

   // scale(mouseX / 1000);

   if (scale < 1) {

     scale = 1
   }
//ADD A SHARE BUTTON

  curve(x1, y1, x2, y2, x3, y3, x4, y4);





  t += 0.0005;

  // clear the background every 500 frames using mod (%) operator
  if (frameCount % 5000 == 0) {
	background(255);
  }


  push();
  fill(231,130,0)
  noStroke();
  textSize(28)
  textFont(myFont);
  text("Share", windowWidth/2 + 400, windowHeight - 100)
  pop();

  image(img, windowWidth/2 + 530, windowHeight - 125, 30, 30)
  image(mig, 50, -50, 200, 200)

}
